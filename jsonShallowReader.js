/**
 * Recursively reads an object and prints its content.
 * Accepts an optional parameter that specifies how many levels deep to read.
 * If a particular branch of the object's height is lower than the level it will not print anything beyond that branch's maximum height.
 *  
 * @param {object}  obj     -   the object to read
 * @param {number}  maxDepth   -   the depth to read
 */
function print (obj, maxDepth){
    //  Verify input
    if (obj !== Object(obj)){
        throw `first argument passed is not an object`;
    }
    if (maxDepth && (!Number.isInteger(maxDepth) || maxDepth < 0)){
        throw `second argument passed is not a valid integer`;
    }
    let printableObj = {};
    if (maxDepth && Number.isInteger(maxDepth)){
        let level = 0;
        printableObj = recursiveDiscovery(obj, maxDepth, level);
    }else{
        printableObj = obj;
    }
    
    return JSON.stringify(printableObj, null, 2);
}

function recursiveDiscovery(obj, maxDepth, level){
    if (obj !== Object(obj)){   return obj; }
    level++;
    if (level > maxDepth){  
        return "...";
    }else{
        let keys = Object.keys(obj);
        let temp = {};
        keys.forEach((key)=>{
            temp[key] = recursiveDiscovery(obj[key], maxDepth, level);
        });
        return temp;
    }
}

exports.print = print;