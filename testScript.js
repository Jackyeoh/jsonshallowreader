const reader = require(`./jsonShallowReader.js`);

let baseObj = {
    attr1:100,
    attr2:"hi"
}
let obj = {
    ...baseObj,
    attr03:{
        ...baseObj,
        attr13:{
            ...baseObj
        }
    }
};

console.log(`Read default`);
console.log(reader.print(obj));

console.log(`Read level1`);
console.log(reader.print(obj, 1));

console.log(`Read level2`);
console.log(reader.print(obj, 2));

console.log(`Read level3`);
console.log(reader.print(obj, 3));