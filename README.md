# JSON Shallow Reader

This tools are created mainly to tackle the difficulties faced when exploring the structure of a circular js object.
It achieves this by allowing users to selectively pretty print an object up certain levels of depth.

I might consider implement a function to flatten an object in the future but this is it for now.

## Getting Started

The only important files are the jsonShallowReader.js. You can choose to only download that file or clone this entire repository.
The testScript.js is a nodejs program that shows how one can use the jsonShallowReader.

### Prerequisites

The tools have zero external dependecies but if you wish to run the testScript.js you need to have nodejs installed.

You can get it here: https://nodejs.org/en/download/

### Installing

To use this tools:
1. download jsonShallowReader.js (or the entire repository, whichever suits you).
2. Import the jsonShallowReader into your nodejs program. Example: "const reader = require('./jsonShallowReader.js');
3. Use the tool by calling "reader.print(jsonObject, depth);"

## Running the tests

To run the test script make sure you have nodejs installed.

On the root directory run the following line:
"node testScript"

You should see the following results:
Read default
{
  "attr1": 100,
  "attr2": "hi",
  "attr03": {
    "attr1": 100,
    "attr2": "hi",
    "attr13": {
      "attr1": 100,
      "attr2": "hi"
    }
  }
}
Read level1
{
  "attr1": 100,
  "attr2": "hi",
  "attr03": "..."
}
Read level2
{
  "attr1": 100,
  "attr2": "hi",
  "attr03": {
    "attr1": 100,
    "attr2": "hi",
    "attr13": "..."
  }
}
Read level3
{
  "attr1": 100,
  "attr2": "hi",
  "attr03": {
    "attr1": 100,
    "attr2": "hi",
    "attr13": {
      "attr1": 100,
      "attr2": "hi"
    }
  }
}


## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.


## Authors

* **Jackyeoh** - *Initial work* - [Jackyeoh](https://gitlab.com/Jackyeoh)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details